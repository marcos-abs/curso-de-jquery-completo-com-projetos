# seletores-jquery

## Exemplos de como utilizar seletores dentro do jQuery

tags: #jQuery, #seletor, #selector

### fonte: [index.html](index.html)

#### Seletor de atributos

```jquery

$('.header-border li [href]').text('link');

```

#### Veja o exemplo 1

![default](2021-09-15-12-28-52.png)

1. em vermelho o comando selecionado
2. em ciano o resultado do comando

#### Seletor de multiplos atributos

```jquery

$('.header-border li [href][title]').text('link');

```

#### Veja o exemplo 2

![multiplos-atributos](2021-09-15-12-38-12.png)

1. em vermelho o comando selecionado
2. em ciano o resultado do comando

#### Verificando o valor de um atributo

```jquery

$('.header-border li [href="about.html"]').text();

```

#### Veja o exemplo 3

![valor-atributo](2021-09-15-16-04-26.png)

1. em vermelho o comando selecionado
2. em ciano o resultado do comando

#### Verificando os atributos que começam com uma determinada letra

```jquery

$('.header-border li [href^="i"]').text();

```

#### Veja o exemplo 4

![valor-atributo](2021-09-15-16-06-46.png)

1. em vermelho o comando selecionado
2. em ciano o resultado do comando

```jquery

$('.header-border li [href$=".html"]').text();

```

#### Veja o exemplo 5

![valor-atributo](2021-09-15-16-09-14.png)

1. em vermelho o comando selecionado
2. em ciano o resultado do comando

```jquery

$('.header-border li [href*="about"]').text();

```

#### Veja o exemplo 6

![valor-atributo](2021-09-15-16-14-16.png)

1. em vermelho o comando selecionado
2. em ciano o resultado do comando

#### Aula: 23. Seletores - Criando nossos próprios filtros

![figure1](2021-11-09-21-30-21.png)

Da onde veio a expressão `$(el).data('ano')` ?
R: veja a linha em destaque abaixo:

![figure2](2021-11-09-21-34-46.png)

da string `data-ano="1983"` por exemplo, onde `data` corresponde ao `.data` no jQuery e o `-ano` ao elemento `'ano'` dentro dos parenteses, retornando o valor `"1983"`, como mostrado no exemplo acima
