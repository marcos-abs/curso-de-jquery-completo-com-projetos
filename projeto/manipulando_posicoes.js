/**
 * *****
 * File: manipulando_posicoes.js
 * Project: Curso Jquery completo - Hcode
 * Path: /home/desenvolvedor/cursos/curso-jquery-hcode/projeto
 * File Created: Monday, 21 March 2022 13:35:47
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 21 March 2022 13:35:48
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 31. Mudando posições dos elementos
 * *****
 */
// mostra a posição (tendo como base a tela) do segundo elemento do formulário
// $('form .field:eq(1)').offset()

// altera a posição (tendo como base a tela) do primeiro elemento do formulário
// $('form .field:eq(0)').offset({
//     top: 35,
//     left: 35
// })

// mostra (apenas) a posição (tendo como base o elemento pai) do segundo elemento do formulário
// $('form .field:eq(1)').position()

// mostra a posição(estado) da barra de rolagem (vertical)
// $(document).scrollTop()

// altera a posição(estado) da barra de rolagem (vertical)
// $(document).scrollTop(300)

// mostra a posição(estado) da barra de rolagem (horizontal)
// $(document).scrollLeft()

// altera a posição(estado) da barra de rolagem (horizontal)
// $(document).scrollLeft(300)
