/**
 * *****
 * File: manipulando_dados.js
 * Project: Curso Jquery completo - Hcode
 * Path: /home/desenvolvedor/cursos/curso-jquery-hcode/projeto
 * File Created: Monday, 21 March 2022 10:16:05
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 21 March 2022 10:16:25
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 29. Armazenando dados nos elementos
 * *****
 */
$('.cars_list li:first').data('ano', '1990');
$('.cars_list li').click(function () {
    console.log(`Ess carro é do ano de ${$(this).data('ano')}`);
});
$('.cars_list li:first').removeData('ano'); // só remove as propriedades que fora criadas virtualmente, as demais não são afetadas.
$('.cars_list li:first').data('pais', 'Itália');

$('.cars_list li:first').data('pais', 'Itália');
$('.cars_list li:first').data('pais');
$('.cars_list li:first').removeData('pais'); // só remove as propriedades que fora criadas virtualmente, as demais não são afetadas.
$('.cars_list li:first').data('pais');
