/**
 * *****
 * File: manipulando_valores.js
 * Project: Curso Jquery completo - Hcode
 * Path: /home/desenvolvedor/cursos/curso-jquery-hcode/projeto
 * File Created: Sunday, 20 March 2022 09:37:21
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Sunday, 20 March 2022 09:38:11
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 28. Manipulando valores dos elementos
 * *****
 */
// verificar se existe o elemento selecionado
$('form [type=text]').length;

// idem ao anterior
$('form [type=text]:first').length;

// verificar o conteúdo do elemento selecionado
$('form [type=text]:first').val();

// alterar o conteúdo do elemento selecionado
$('form [type=text]:first').val('John Resig');

// alterando o conteúdo do elemento do tipo data. ***Obs. a data é no formato americano (ISO 8601) ver em: https://pt.wikipedia.org/wiki/ISO_8601
$('form [type=date]').val('1977-05-25'); // Dia do Orgulho Geek - Dia da Toalha
