/**
 * *****
 * File: manipulando_propriedades.js
 * Project: Curso Jquery completo - Hcode
 * Path: /home/desenvolvedor/cursos/curso-jquery-hcode/projeto
 * File Created: Saturday, 19 March 2022 14:14:21
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Saturday, 19 March 2022 14:14:24
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 27. Manipulando propriedades
 * *****
 */
// verificar se existe o elemento
$('input[type="checkbox"]').length;

// verificar se a "propriedade" está "marcada"
$('input[type="checkbox"]').prop('checked'); // resultado: false

// verificar se o "atributo" está "marcado"
$('input[type="checkbox"]').attr('checked'); // resultado: undefined - motivo: este é um caso de "propriedade" e não de "atributo".

// alterar a "propriedade" para "marcada"
$('input[type="checkbox"]').prop('checked', true);

// alterar a "propriedade" para "desmarcada"
$('input[type="checkbox"]').prop('checked', false);

// ***OBSERVAÇÃO IMPORTANTE: só podemos remover propriedade do DOM que forem criadas por nós, ou seja, não podemos remover propriedade geradas pelo navegador. Veja os exemplos abaixo:
// verificar o valor da propriedade "nome" no elemento "h1"
$('h1').prop('nome');

// alterar o valor da propriedade "nome" no elemento "h1" para "titulo"
$('h1').prop('nome', 'título');

// para remover a propriedade "nome" (que foi criada por nós)
$('h1').removeProp('nome');
