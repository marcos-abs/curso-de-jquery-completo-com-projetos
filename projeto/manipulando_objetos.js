/**
 * *****
 * File: manipulando_objetos.js
 * Project: Curso Jquery completo - Hcode
 * Path: /home/desenvolvedor/cursos/curso-jquery-hcode/projeto
 * File Created: Saturday, 19 March 2022 11:04:20
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Saturday, 19 March 2022 11:04:23
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 27. Manipulando propriedades
 * *****
 */

// retorna o caminho da imagem
$('img').attr('src');

// retorna o texto alternativo da imagem
$('img').attr('alt');

// altera o caminho da imagem
$('img').attr('src', 'assets/images/jquery.jpg');

// remove atributo do elemento da página
$('h1').removeAttr('title');
