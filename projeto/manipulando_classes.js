/**
 * *****
 * File: manipulando_classes.js
 * Project: Curso Jquery completo - Hcode
 * Path: /home/desenvolvedor/cursos/curso-jquery-hcode/projeto
 * File Created: Friday, 18 March 2022 15:17:04
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 18 March 2022 15:17:07
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 26. Manipulando atributos
 * *****
 */
// Adicionando uma classe
// $('header .menu ul li:last a').addClass('stylized_menu');

// Removendo uma classe
// $('header .menu ul li:last a').removeClass('stylized_menu');

// Alternando uma classe
$('header .menu ul li a').click(function (event) {
    event.preventDefault();

    $(this).toggleClass('stylized_menu');
});
$('p').each(function () {
    if ($(this).hasClass('paragrafo')) {
        $(this).append('<span style="color:red"> - Eu tenho a class</span>');
    }
});
