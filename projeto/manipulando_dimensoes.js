/**
 * *****
 * File: manipulando_dimensoes.js
 * Project: Curso Jquery completo - Hcode
 * Path: /home/desenvolvedor/cursos/curso-jquery-hcode/.vscode
 * File Created: Monday, 21 March 2022 11:35:29
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 21 March 2022 11:35:31
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 30. Mudando dimensões dos elementos
 * *****
 */
// ver tamanho de um elemento
$('.paragrafo').width();

// alterando o tamanho de um elemento
$('.paragrafo').width(300);

// ver o tamanho do elemento
$('.paragrafo').height();

// altera o tamanho do elemento
$('.paragrafo').height(214);

// ver o tamanho juntamente com o Padding
$('.paragrafo').innerHeight();

// altera o tamanho juntamente com o Padding
$('.paragrafo').innerHeight(214);
