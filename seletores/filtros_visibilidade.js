/**
 * *****
 * File: filtros_visibilidade.js
 * Project: Curso Jquery completo - Hcode
 * File Created: Saturday, 30 October 2021 19:51:03
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Saturday, 30 October 2021 19:51:05
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2021 - 2021 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				aula 19. Seletores - Filtros de Visibilidade ===>>> na seção: 3. Seletores jQuery
 * *****
 */
                // ***** Selecionando os elementos que estão "invisíveis" *****
                // $('.cars_list li:hidden').show()
                // ***** Selecionando os elementos que estão "visíveis" *****
                // $('.cars_list li:visible').hide()
