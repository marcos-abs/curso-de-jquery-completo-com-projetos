/**
 * *****
 * File: seletores_heranca.js
 * Project: Curso Jquery completo - Hcode
 * File Created: Wednesday, 15 September 2021 11:19:22
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 15 September 2021 11:20:15
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2021 - 2021 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula de Seletores por Herança
 * *****
 */
            // Elementos descendentes
            // $('main h1').css('color', 'red');

            // Elemento descendente direto
            // $('main>.title').css('border', 'solid 2px blue');

            // Elemento irmão mais próximo -> +
            // Todos os elementos irmãos -> ~
            // $('.paragrafo~p').css({
            //     'background-color': 'red',
            //     color: 'white',
            // });
