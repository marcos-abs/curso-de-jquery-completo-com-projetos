/**
 * *****
 * File: filtros_conteudo.js
 * Project: Curso Jquery completo - Hcode
 * File Created: Saturday, 30 October 2021 19:08:16
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Saturday, 30 October 2021 19:08:19
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2021 - 2021 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 18. Seletores - Filtros de Conteúdo ===>>> na seção: 3. Seletores jQuery
 * *****
 */
            // ***** Selecionando um elemento pelo seu conteúdo
            // $('.menu li a:contains(About)').css('outline', 'solid red 2px') // contains() é case sensitive
            // ***** Selecionando um elemento que não tem conteúdo
            // $('.cars_list li:empty').text('Vazio').css('background', 'red');
            // $('.cars_list li:empty').text('Vazio').css({
            //     'background': 'red',
            //     'color': 'white'
            // });
            // ***** Selecionando um elemento que possuem conteúdo
            // $('.cars_list li:parent').append('<span style="color: green;"> - Tem valor</span>')
            // Selecionando os elementos que possuem outros elementos
            // $('.cars_list li:has(span)').animate({
            //     fontSize: '30px',
            //     opacity: 0.7
            // });
