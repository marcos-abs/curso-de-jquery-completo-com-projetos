/**
 * *****
 * File: filtros_basicos.js
 * Project: Curso Jquery completo - Hcode
 * File Created: Thursday, 16 September 2021 23:30:22
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 16 September 2021 23:31:24
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2021 - 2021 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula de Filtros Básicos
 * *****
 */
// Selecionando o último elemento
// $('.cars_list li:last').click(function () {
//     alert('ok');
// });

// Selecionando o primeiro elemento
// $('.cars_list li:first').click(function () {
//     console.log('Cliquei no primeiro item');
// });

// Selecionando itens de maneira alternada - Efeito formulário zebrado - começando pelos impares
// $('.cars_list li:even').css('background-color', '#ccc');

// Selecionando itens de maneira alternada - Efeito formulário zebrado - começando pelos pares
// $('.cars_list li:odd').css('background-color', '#ccc');

// Selecionando todos os itens abaixo de outro (comando depreciado ver em https://api.jquery.com/gt-selector/#gt1)
// $('.cars_list li:gt(2)').css('opacity', '0.3');

// Selecionando todos os itens acima de outro (comando depreciado ver em https://api.jquery.com/gt-selector/#gt1)
// $('.cars_list li:lt(2)').css('opacity', '0.3');
