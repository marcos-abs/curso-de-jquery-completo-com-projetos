/**
 * *****
 * File: seletores_atributos.js
 * Project: Curso Jquery completo - Hcode
 * File Created: Thursday, 16 September 2021 16:28:23
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 16 September 2021 16:30:32
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2021 - 2021 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula de Seletores de Atributos
 * *****
 */
// cSpell:ignore debugando
    // Verificando se possui um atributo
    // $('.header-border li [href]').text('link');

    // Verificando se possui mais de um atributo
    // $('.header-border li [href][title]').text('link');

    // Verificando um valor para o atributo (debugando)
    // $('.header-border li [href="about.html"]').text();

    // Verificando os atributos que começam com uma determinada letra
    // $('.header-border li [href^="i"]').text()

    // Verificando os atributos que terminam com uma determinada string
    // $('.header-border li [href$=".html"]').text()

    // Verificando os atributos que contenham uma determinada string
    // $('.header-border li [href*="about"]').text()
