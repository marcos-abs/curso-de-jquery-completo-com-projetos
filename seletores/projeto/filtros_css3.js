/**
 * *****
 * File: filtros_css3.js
 * Project: Curso Jquery completo - Hcode
 * File Created: Saturday, 30 October 2021 17:36:37
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Saturday, 30 October 2021 17:37:08
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2021 - 2021 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Codigos testados da aula 17
 * *****
 */
                // Selecionando o último elemento, mesmo que seja mais de um caso (função do jQuery)
                // $('.cars_list li:last').css('text-decoration', 'underline');
                // Selecionando o último elemento, mesmo que seja mais de um caso (função do css3)
                // $('.cars_list li:last-child').css(
                //     'text-decoration',
                //     'underline',
                // );
                // ***** Selecionando o antepenultimo elemento, mesmo que seja mais de um caso (função do css3) *****
                // $('.cars_list li:nth-last-child(3)').css('font-weight', 'bold');
                // ***** Selecionando o primeiro elemento, mesmo que seja mais de um caso (função do jQuery) *****
                // $('.cars_list li:first').css('font-size', '25px');
                // ***** Selecionando o primeiro elemento, mesmo que seja mais de um caso (função do jQuery) *****
                // $('.cars_list li:first-child').css('font-size', '25px');
                // ***** Selecionando o primeiro elemento, somente os primeiros casos (função do css3) *** a posição começa em 1 e não em 0 como nos array *****
                // $('.cars_list li:nth-child(1)').css('background-color', '#ccc');
                // ***** Selecionando o primeiro elemento, somente um caso *****
                // $('.cars_list li:eq(0)').css('background-color', '#ccc');
                // ***** Selecionando os elementos de maneira alternada impar (função do css3) *****
                // $('.cars_list li:nth-child(odd)').css('background-color', '#ccc');
                // ***** Selecionando os elementos de maneira alternada par (função do css3) *****
                // $('.cars_list li:nth-child(even)').css('background-color', '#ccc');
                // ***** Selecionando o primeiro elemento, mesmo que seja mais de um caso (função do css3) *****
                // $('.cars_list li:even').css('background-color', '#ccc');
