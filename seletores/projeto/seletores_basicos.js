/**
 * *****
 * File: seletores_basicos.js
 * Project: Curso Jquery completo - Hcode
 * File Created: Wednesday, 15 September 2021 09:57:32
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 15 September 2021 09:58:11
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2021 - 2021 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula de Seletores básicos
 * *****
 */
            // $(document).ready(function () {
            //     $('#toggle_cars').click(function () {
            //         let hiddenCars = $('.cars_list li:hidden').length;
            //         let method = hiddenCars > 0 ? 'show' : 'hide';

            //         eval(`$('.cars_list li').${method}()`);
            //     });

            //     $.expr[':'].carsCentury21 = function (el) {
            //         return $(el).data('ano') > 2000;
            //     };

            //     $.expr[':'].carsBeforeYear = $.expr.createPseudo(function (
            //         year,
            //     ) {
            //         return function (el) {
            //             return $(el).data('ano') < year;
            //         };
            //     });

            // Criando nosso próprio filtro
            // $('.cars_list li:carsCentury21').css('font-size', '30px')
            // });

            // Exemplos dados em aula:
            // por Nome da tag -> $('h1').css('color', 'red');

            // por ID -> $('#logo').html('Negócios')

            // por classe ->
            // $('.paragrafo').animate(
            //     {
            //         fontSize: '50px',
            //         opacity: 0.7,
            //     },
            //     5000,
            // );

            // Seletor composto
            // $('header nav.menu').css('border', 'solid 1px orange');

            // Multiplos seletores
            // $('form, section').css('background-color', '#ccc');
