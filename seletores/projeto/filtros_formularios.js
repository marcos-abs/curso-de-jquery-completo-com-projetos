$(document).ready(function () {
    $('[type=radio]').change(function () {
        let value = $('input:checked').val();
        let gender = value == 'M' ? 'Masculino' : 'Feminino';
        alert(`Você selecionou o sexo ${gender}`);
    });

    $('input').focus(function () {
        $('input:focus').val('Olá, você clicou aqui!');
    });

    $('#register :submit').click(function (event) {
        event.preventDefault();
        console.log('Você clicou no botão submit');
    });
});
// Selecionando o botão submit
// $('#register :submit').css('background-color', 'red');

// Selecionando os elementos desabilitados
// $('#register :disabled').val('anthonny@hcode.com.br').prop('disabled', false);

// $('#toggle_cars').click(function () {
//     let hiddenCars = $('.cars_list li:hidden').length;
//     // console.log('hiddenCars: ', hiddenCars); // debugando a variável
//     let method = (hiddenCars > 0) ? 'show' : 'hide';
//     eval(`$('.cars_list li').${method}()`);
// });
// Selecionando um checkbox
// $('#register :checkbox').trigger('click')

// $('#register button').css('opacity', '0.5'); // funciona em <button> somente
// $('#register :button').css('opacity', '0.5'); // funciona em <input> e <button>"

// Selecionando um botão
// $('#register :button').text();

// filtro1 customizado
$.expr[':'].carsCentury21 = function (el) {
    return $(el).data('ano') > 2000;
};

// filtro2 customizado
$.expr[':'].carsBeforeYear = $.expr.createPseudo(function (year) {
    return function (el) {
        return $(el).data('ano') < year;
    };
});

// utilizando nosso próprio filtro2.1
// $('.cars_list li:carsBeforeYear(1990)').css('color', 'orange')

// utilizando nosso próprio filtro2.2
$('.cars_list li:carsBeforeYear(1950)').css('color', 'red');

// utilizando nosso próprio filtro1
// $('.cars_list li:carsCentury21').css('font-size', '30px')

$('#toggle_cars').click(function () {
    let hiddenCars = $('.cars_list li:hidden').length;
    let method = hiddenCars > 0 ? 'show' : 'hide';

    eval(`$('.cars_list li').${method}()`); // jshint ignore:line
});
